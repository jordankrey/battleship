﻿using NUnit.Framework;
using Battleship.Game.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;

namespace Battleship.Game.Player.Tests
{
    [TestFixture()]
    public class AIPlayerTests
    {
        [Test()]
        public void GetNextGuessTest()
        {
            //GIVEN a new AI Player
            var player = new AIPlayer();

            //WHEN we get the next guess
            var coord = player.GetNextGuess();

            //THEN the guess X coordinate should be between 1-10
            //AND the guess Y coordinate should be between A-J
            coord.X.Should().BeInRange(1, 10);
            coord.Y.Should().BeInRange('A', 'J');
        }
    }
}