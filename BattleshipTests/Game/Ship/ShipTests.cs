﻿using NUnit.Framework;
using Battleship.Game.Ship;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Battleship.Game.Board;
using FluentAssertions;

namespace Battleship.Game.Ship.Tests
{
    [TestFixture()]
    public class ShipTests
    {
        [Test()]
        [TestCase(2, 'A', true)]
        [TestCase(1, 'A', true)]
        [TestCase(3, 'A', true)]
        [TestCase(4, 'A', false)]
        [TestCase(1, 'B', false)]
        public void IsHitTest(int x, char y, bool shouldBeHit)
        {
            //GIVEN a ship with coordinates from A1 to A3
            var ship = new Ship()
            {
                Start = new Coordinate(1, 'A'),
                End = new Coordinate(3, 'A')
            };

            //WHEN isHit is called for valid coordinate
            var hit = ship.IsHit(new Coordinate(x, y));

            //THEN isHit should be true
            hit.Should().Be(shouldBeHit);
        }

        [Test()]
        public void IsSunkTest()
        {
            //GIVEN a ship that is sunk
            var ship = new Ship()
            {
                Start = new Coordinate(1, 'A'),
                End = new Coordinate(3, 'A')
            };

            var guesses = new Dictionary<Coordinate, bool>()
            {
                {new Coordinate(1, 'A'), true},
                {new Coordinate(2, 'A'), true},
                {new Coordinate(3, 'A'), true},
            };

            //WHEN isSunk is called with the guesses
            var sunk = ship.IsSunk(guesses);

            //THEN isSunk should be true
            sunk.Should().BeTrue();


        }
    }
}