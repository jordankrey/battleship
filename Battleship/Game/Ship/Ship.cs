﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Battleship.Game.Board;

namespace Battleship.Game.Ship
{
    public class Ship : IShip
    {

        public Coordinate Start{ get; set; }

        public Coordinate End { get; set; }

        public bool IsSunk(IDictionary<Coordinate, bool> guesses)
        {
            var isSunk = true;

            if (Start.Y == End.Y)
            {
                var currentX = Start.X;
                while (currentX <= End.X)
                {
                    isSunk &= guesses.ContainsKey(new Coordinate(currentX, Start.Y));
                    currentX++;
                }
            }
            else
            {
                var currentY = Start.Y;
                while (currentY <= End.Y)
                {
                    isSunk &= guesses.ContainsKey(new Coordinate(Start.X, currentY));
                    currentY++;
                }
            }

            return isSunk;
        }

        /// <summary>
        /// Returns true if the coordinate is within the start/end coordinates of the ship
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public bool IsHit(Coordinate c)
        {
            return Start.X <= c.X 
                && End.X >= c.X 
                && Start.Y <= c.Y
                && End.Y >= c.Y;
            
        }
    }
}
