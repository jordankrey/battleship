﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Battleship.Game.Board;

namespace Battleship.Game.Ship

{
    /// <summary>
    /// Defines a ship along and x and y axis
    /// Ships cannot be diagonal
    /// </summary>
    public interface IShip
    {

        /// <summary>
        /// Returns true if the ship is hit with the coordinates provided
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        bool IsHit(Coordinate c);

        /// <summary>
        /// Returns true if the ship is sunk (all coordinates are hit)
        /// </summary>
        /// <param name="guesses"></param>
        /// <returns></returns>
        bool IsSunk(IDictionary<Coordinate, bool> guesses);

    }
}
