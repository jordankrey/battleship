﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Battleship.Game.Board;

namespace Battleship.Game.View
{
    public class BoardView
    {
        private const string prefix = "  ";
        private const char noGuess = '.';
        private const char hit = 'x';
        private const char miss = 'o';
        private const char ship = 's';
        private const char shipHit = '*';


        public static string Draw(IBoard board)
        {
            StringBuilder graph = new StringBuilder();
            graph.AppendLine();
            graph.Append(prefix).AppendLine(@" 1  2  3  4  5  6  7  8  9  10");

            var currentY = board.GetStart().Y;
            var guesses = board.GetGuesses();
            var ships = board.GetShips();

            while (currentY <= board.GetEnd().Y)
            {
                graph.AppendLine();
                graph.Append(currentY);
                var currentX = board.GetStart().X;

                while (currentX <= board.GetEnd().X)
                {
                    var coord = new Coordinate(currentX, currentY);
                    var guessed = guesses.ContainsKey(coord);
                    var isShip = board.IsHit(coord);
                    
                    
                    if (!guessed)
                    {
                        if (isShip)
                        {
                            graph.Append(prefix).Append(ship);
                        }
                        else
                        {
                            graph.Append(prefix).Append(noGuess);
                        }
                    }
                    else if (guesses[coord])
                    {
                        if (isShip)
                        {
                            graph.Append(prefix).Append(shipHit);
                        }
                        else
                        {
                            graph.Append(prefix).Append(hit);
                        }
                    }
                    else
                    {
                        if (isShip)
                        {
                            graph.Append(prefix).Append(ship);
                        }
                        else
                        {
                            graph.Append(prefix).Append(miss);
                        }
                    }

                    currentX++;
                }

                currentY++;
            }

            return graph.ToString();
        }

    }
}
