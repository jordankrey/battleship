﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Battleship.Game.Player;
using Battleship.Game.View;

namespace Battleship.Game
{
    public class GameEngine
    {

        public IPlayer HumanPlayer { get; private set;  }
        public IPlayer AIPlayer { get; private set; }

        private IPlayer NextTurn { get; set; }
        private IPlayer PreviousTurn { get; set; }


        public void BeginGame()
        {
            HumanPlayer = new HumanPlayer();
            AIPlayer = new AIPlayer();

            HumanPlayer.SetupPlayerBoard();
            AIPlayer.SetupPlayerBoard();

            NextTurn = HumanPlayer;
            PreviousTurn = AIPlayer;

            Console.WriteLine("Get ready to battle!");
        }

        /// <summary>
        /// Plays the next turn of the game
        /// </summary>
        /// <returns>Returns false if the game is over</returns>
        public bool PlayNextTurn()
        {
            var currentTurn = NextTurn;

            var coor = currentTurn.GetNextGuess();

            var result = PreviousTurn.RegisterOpponentGuess(coor);

            currentTurn.RegisterGuessResult(coor, result);

            var isWon = !PreviousTurn.StillAfloat();
            if (isWon)
            {
                Console.WriteLine("Game over!");
                Console.ReadLine();
                return false;
            }
            NextTurn = PreviousTurn;
            PreviousTurn = currentTurn;

            return true;
        }
    }
}
