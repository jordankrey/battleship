﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship.Game.Board
{
    /// <summary>
    /// Defines a coordinates on a Battleship Board
    /// </summary>
    public class Coordinate
    {
        private int XCoord;
        private char YCoord;

        public Coordinate(int x, char y)
        {
            X = x;
            Y = y;
        }

        public int X
        {
            get { return XCoord; }
            set { XCoord = value;  }
        }

        public char Y
        {
            get { return YCoord; }
            set { YCoord = char.ToUpperInvariant(value); }
        }

        public override string ToString()
        {
            return string.Format("{0}{1}", YCoord, XCoord);
        }

        public override bool Equals(object obj)
        {
            return this.ToString().Equals(obj.ToString());
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
    }
}
