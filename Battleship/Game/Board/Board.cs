﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Battleship.Game.Ship;

namespace Battleship.Game.Board
{
    public class Board : IBoard
    {
        
        public IDictionary<Coordinate, bool> Guesses = new Dictionary<Coordinate, bool>();
        public IList<IShip> Ships = new List<IShip>();

        public Coordinate GetStart()
        {
            return new Coordinate(1, 'A');
        }

        public Coordinate GetEnd()
        {
            return new Coordinate(10, 'J');
        }

        public void AddShip(IShip ship)
        {
            Ships.Add(ship);
        }

        public void RegisterGuessResult(Coordinate guessedCoordinate, bool wasHit)
        {
            Guesses[guessedCoordinate] =  wasHit;
        }

        public IDictionary<Coordinate, bool> GetGuesses()
        {
            return Guesses;
        }

        public bool IsHit(Coordinate coord)
        {
            return Ships.Any(s => s.IsHit(coord));
        }

        public IList<IShip> GetShips()
        {
            return Ships;
        }

        public bool IsAShipStillAfloat()
        {
            return Ships.Any(s => !s.IsSunk(Guesses));
        }
    }
}
