﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Battleship.Game.Ship;

namespace Battleship.Game.Board
{
    public interface IBoard
    {
        Coordinate GetStart();
        Coordinate GetEnd();
        IDictionary<Coordinate, bool> GetGuesses();
        bool IsHit(Coordinate coord);
        bool IsAShipStillAfloat();
        IList<IShip> GetShips();

        /// <summary>
        /// Puts a ship on the board 
        /// </summary>
        /// <param name="ship"></param>
        void AddShip(IShip ship);

        void RegisterGuessResult(Coordinate guessedCoordinate, bool wasHit);


    }
}
