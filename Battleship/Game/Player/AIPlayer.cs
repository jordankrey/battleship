﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Battleship.Game.Board;
using Battleship.Game.Ship;

namespace Battleship.Game.Player
{
    public class AIPlayer : IPlayer
    {

        private IBoard PlayerBoard = new Board.Board();
        private IBoard OpponentBoard = new Board.Board();

        public IEnumerable<IShip> GetShips()
        {
            throw new NotImplementedException();
        }

        public Coordinate GetNextGuess()
        {

            while (true)
            {
                var potentialGuess = new Coordinate
                    (
                    new Random().Next(10) + 1, //offset so that it is from 1-10
                    (char) ('A' + new Random().Next(10)) //A-J
                    );

                if (!OpponentBoard.GetGuesses().ContainsKey(potentialGuess))
                {
                    return potentialGuess;
                }
            }


        }

        public void RegisterGuessResult(Coordinate guessedCoordinate, bool wasHit)
        {
            OpponentBoard.RegisterGuessResult(guessedCoordinate, wasHit);
        }

        public bool RegisterOpponentGuess(Coordinate guessedCoordinate)
        {
            var isHit = PlayerBoard.IsHit(guessedCoordinate);
            PlayerBoard.RegisterGuessResult(guessedCoordinate, isHit);
            return isHit;
        }

        public IBoard GetPlayerBoard()
        {
            return PlayerBoard;
        }

        public void SetupPlayerBoard()
        {
            //Destroyer
            PlayerBoard.AddShip(new Ship.Ship()
            {
                Start = new Coordinate(1, 'A'),
                End = new Coordinate(2, 'A')
            });

            /*
            //Submarine
            PlayerBoard.AddShip(new Ship.Ship()
            {
                Start = new Coordinate(3, 'H'),
                End = new Coordinate(3, 'J')
            });

            //Cruiser
            PlayerBoard.AddShip(new Ship.Ship()
            {
                Start = new Coordinate(1, 'C'),
                End = new Coordinate(3, 'C')
            });

            //Battleship
            PlayerBoard.AddShip(new Ship.Ship()
            {
                Start = new Coordinate(10, 'E'),
                End = new Coordinate(10, 'H')
            });

            //Carrier
            PlayerBoard.AddShip(new Ship.Ship()
            {
                Start = new Coordinate(4, 'F'),
                End = new Coordinate(8, 'F')
            });
            */
    }

        public bool StillAfloat()
        {
            return PlayerBoard.IsAShipStillAfloat();
        }
    }
}
