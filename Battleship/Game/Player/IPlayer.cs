﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Battleship.Game.Board;
using Battleship.Game.Ship;

namespace Battleship.Game.Player
{
    /// <summary>
    /// A player of the game
    /// </summary>
    public interface IPlayer
    {

        /// <summary>
        /// Gets the next guess from the player
        /// </summary>
        /// <returns></returns>
        Coordinate GetNextGuess();

        /// <summary>
        /// Tells the player whether a guess was a hit or a miss
        /// </summary>
        /// <param name="guessedCoordinate"></param>
        /// <param name="wasHit"></param>
        void RegisterGuessResult(Coordinate guessedCoordinate, bool wasHit);

        /// <summary>
        /// Tells the player that an opponent has made a guess
        /// And allows the player to respond with the result
        /// </summary>
        /// <param name="guessedCoordinate"></param>
        /// <param name="wasHit"></param>
        bool RegisterOpponentGuess(Coordinate guessedCoordinate);

        /// <summary>
        /// Returns the current representation of the player's board
        /// </summary>
        /// <returns></returns>
        IBoard GetPlayerBoard();

        /// <summary>
        /// Add ships to the board
        /// </summary>
        void SetupPlayerBoard();

        /// <summary>
        /// Returns true if the player still has ships that are afloat
        /// </summary>
        /// <returns></returns>
        bool StillAfloat();

    }
}
