﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Battleship.Game.Board;
using Battleship.Game.Ship;
using Battleship.Game.View;

namespace Battleship.Game.Player
{
    public class HumanPlayer : IPlayer
    {

        private IBoard PlayerBoard = new Board.Board();
        private IBoard OpponentBoard = new Board.Board();

        public IEnumerable<IShip> GetShips()
        {
            throw new NotImplementedException();
        }

        public Coordinate GetNextGuess()
        {
            Console.WriteLine("Your board:");
            Console.WriteLine(BoardView.Draw(PlayerBoard));

            Console.WriteLine("Your opponent's board:");
            Console.WriteLine(BoardView.Draw(OpponentBoard));

            var badGuess = true;
            var x = -1;
            var y = 'z';

            while (badGuess)
            {
                Console.WriteLine("What's your next guess (i.e. A1)?");
                var unverifiedGuess = Console.ReadLine();

                if (!String.IsNullOrWhiteSpace(unverifiedGuess) && unverifiedGuess.Length >= 2)
                {
                    x = int.Parse(unverifiedGuess.Substring(1));
                    y = unverifiedGuess[0];
                    badGuess = false;
                }
            }
            return new Coordinate(x, y);
        }

        public void RegisterGuessResult(Coordinate guessedCoordinate, bool wasHit)
        {
            if (wasHit)
            {
                Console.WriteLine("Hit!");
            }
            else
            {
                Console.WriteLine("Miss.");
            }

            OpponentBoard.RegisterGuessResult(guessedCoordinate, wasHit);
        }

        public bool RegisterOpponentGuess(Coordinate guessedCoordinate)
        {
            Console.WriteLine("Opponent guessed: " + guessedCoordinate);

            
            var badResponse = true;
            while (badResponse)
            {
                Console.WriteLine("Response hit or miss?");
                var response = Console.ReadLine();

                if (!string.IsNullOrWhiteSpace(response))
                {
                    response = response.ToLowerInvariant();
                    if (response.Equals("miss"))
                    {
                        PlayerBoard.RegisterGuessResult(guessedCoordinate, false);
                        return false;
                    } else if (response.Equals("hit"))
                    {
                        PlayerBoard.RegisterGuessResult(guessedCoordinate, true);
                        return true;
                    } 
                }
            }

            PlayerBoard.RegisterGuessResult(guessedCoordinate, false);
            return false;
        }

        public IBoard GetPlayerBoard()
        {
            return PlayerBoard;
        }

        public void SetupPlayerBoard()
        {
            //Destroyer
            PlayerBoard.AddShip(new Ship.Ship()
            {
                Start = new Coordinate(1, 'A'),
                End = new Coordinate(2, 'A')
            });


            //Submarine
            PlayerBoard.AddShip(new Ship.Ship()
            {
                Start = new Coordinate(3, 'H'),
                End = new Coordinate(3, 'J')
            });

            //Cruiser
            PlayerBoard.AddShip(new Ship.Ship()
            {
                Start = new Coordinate(1, 'C'),
                End = new Coordinate(3, 'C')
            });

            //Battleship
            PlayerBoard.AddShip(new Ship.Ship()
            {
                Start = new Coordinate(10, 'E'),
                End = new Coordinate(10, 'H')
            });

            //Carrier
            PlayerBoard.AddShip(new Ship.Ship()
            {
                Start = new Coordinate(4, 'F'),
                End = new Coordinate(8, 'F')
            });

        }

        public bool StillAfloat()
        {
            return PlayerBoard.IsAShipStillAfloat();
        }
    }
}
