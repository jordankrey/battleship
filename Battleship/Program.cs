﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Battleship.Game;

namespace Battleship
{
    class Program
    {
        static void Main(string[] args)
        {
            var gameEngine = new GameEngine();

            gameEngine.BeginGame();

            while (gameEngine.PlayNextTurn()) ;
        }
    }
}
